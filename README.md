# Microsserviço request na API de moradores

![N|HTML5](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white) ![N|HTML5](https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white) ![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)

> Microsserviço que contém rotas para consulta na API de moradores, fornecendo assim uma API da isket para empresas realizarem as consultas. Todas as rotas precisam de autenticação com email e senha referente à algum usuário da plataforma. 

## Contexto
Esse projeto é um projeto Node.js que usa Express.js, Typescript e Axios para providenciar rotas para empresas consumirem uma API da isket para realizarem consulta de moradores.

##### Links para as documentações dos principais pacotes usados:
- Express.js ➡️ https://expressjs.com/en/5x/api.html
- Axios ➡️ https://axios-http.com/docs/example
- Typescript ➡️ https://www.typescriptlang.org/docs/
- Mongo DB ➡️ https://www.mongodb.com/docs/
- Mongoose ➡️ https://mongoosejs.com/docs/5.x/

## Lista de rotas:

Para fazer quaquer request para a API, será necessário enviar uma autenticação com as credenciais no HEADER de cada request, sendo esses o email e senha para acesso na plataforma Isket (nova).

Sendo o email `teste@teste.com` e a senha `1234`, juntar email e senha com dois pontos, da seguinte maneira: `teste@teste.com:1234` e depois codificar como [base64](https://www.base64encode.org/), ficando assim: `dGVzdGVAdGVzdGUuY29tOjEyMzQ=`. 

O header a ser enviado fica então:

```json
{
	"authorization": "bearer dGVzdGVAdGVzdGUuY29tOjEyMzQ="
}
```

Exemplo de request com curl:

```sh
curl https://moradores.isket.com.br/consulta/Pessoa/1234 -H "authorization: bearer dGVzdGVAdGVzdGUuY29tOjEyMzQ="
```


Caso as credenciais não sejam enviadas, receberá um erro de status 403 (Forbidden) como esse:

```json
{
	"message": "Usuário sem credenciais."
}
```

<!-- **Observação:** O arquivo `Insomnia.json`, localizado no root do projeto contém exemplos das requests feitas pela API. -->

##  <span style="background-color:blue">GET</span>  /consulta/:search/:document
Rota utilizada para realizar pesquisas de pessoas/empresa por documento.

- `:search`: "Pessoa" ou "Empresa"
- `:document`: cpf ou cnpj (apenas números)

Exemplo de request: `/consulta/Pessoa/00000000000`

### Response de sucesso
Se existir uma conta no banco de dados (Mongo DB), o seguinte objeto é retornado:
```json
{
	"message": "Pesquisa realizada com sucesso!",
	"data": "..."
}
```
Caso haja erro na verificação de token, autenticação do usuário, ou falha na busca, um erro é retornado.


##  <span style="background-color:blue">POST</span>  /listar/:search
Rota utilizada para realizar pesquisas de listagem de pessoas/empresa.

- `:search`: "Pessoa" ou "Empresa"

Exemplo de request: `/listar/Pessoa`


### Request
Body: 
```json
{
	"CEP": "",
	"Fone": "",
	"Nome": "",
	"NumeroEnd": "",
	"cidade": "Curitiba",
	"ddd": "",
	"email": "",
	"endereco": "rua conselheiro laurindo",
	"uf": "pr",
	"complemento": ""
}
```


### Response de sucesso
Se existir uma conta no banco de dados (Mongo DB), o seguinte objeto é retornado:
```json
{
	"message": "Pesquisa realizada com sucesso!",
	"data": "..."
}
```
Caso haja erro na verificação de token, autenticação do usuário, ou falha na busca, um erro é retornado.


##  <span style="background-color:blue">POST</span>  /avaliar
Rota utilizada para avaliar um imóvel baseado nas suas características.

Exemplo de request: `/avaliar`

### Request
Body: 
```json
{
	"area_util": 68,
	"area_total":122,
	"finalidade": "Residencial",
	"tipo_negocio": "Venda",
	"tipo_imovel": "Apartamento",
	"quarto": 2,
	"banheiro": 2,
	"garagem": 1,
	"long": -27.59075542816697, 
	"lat": -48.52071765766073,
	"valor":  700000,
	"condominio": null
}
```